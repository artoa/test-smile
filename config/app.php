<?php

return [
    /* DB parameters */
    'DB_HOST' => '127.0.0.1',
    'DB_PORT' => '3306',
    'DB_DATABASE' => 'ts',
    'DB_USERNAME' => 'root',
    'DB_PASSWORD' => 'admin',

    /* twig parameters */
    'TWIG_VIEW_FOLDER' => ROOT . 'app/View/'
];