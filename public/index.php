<?php
session_start();
ini_set('display_errors', false);

error_reporting(-1);

register_shutdown_function(function()
{
    if ((!is_null($err = error_get_last())) && (!in_array($err['type'], array (E_NOTICE, E_WARNING))))
    {
        $_SESSION['error'] = $err['message'];

        header("HTTP/1.1 301 Moved Permanently");
        header("Location: /", true, 301);
        exit();
    }
});

define('ROOT', __DIR__ . '/../');

require ROOT . '/vendor/autoload.php';

if (!function_exists('dd')) {

    /**
     * Variable dump and kill process
     *
     * @param $item
     */
    function dd ($item)
    {
        echo '<pre>';
        var_dump($item);
        echo '</pre>';
        exit();
    }
}

$kernel = new \app\Kernel(new \app\Instruments\Request());
$kernel->response();