####require:
- php: 7+
- twig/twig: 2.0,
- parsecsv/php-parsecsv: 1.0

#### configuration:
- File `config/app.php` contains control parameters
- Folder `migrations` contain migration `.sql` files
 