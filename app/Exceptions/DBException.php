<?php

namespace app\Exceptions;


use Throwable;

class DBException extends Exception
{
    public function __construct($message = "Data base error", $code = 500, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

}