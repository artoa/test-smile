<?php

namespace app;

use app\Controllers\ExceptionController;
use app\Controllers\HomeController;
use app\Exceptions\Exception;
use app\Exceptions\PathNotFoundException;
use app\Instruments\PDORepository;
use app\Instruments\Request;

class Kernel
{
    /**
     * @var Request
     */
    private $request;

    /**
     * @var array
     */
    private static $config;

    /**
     * @var PDORepository|null
     */
    private $pdoRepository;

    /**
     * Kernel constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Kernel response
     */
    public function response()
    {
        try {
            //todo: router
            if ($this->request && $this->request->getServerElement('REQUEST_URI') == '/') {
                $this->pdoRepository = new PDORepository(
                    self::config('DB_HOST'),
                    self::config('DB_PORT'),
                    self::config('DB_DATABASE'),
                    self::config('DB_USERNAME'),
                    self::config('DB_PASSWORD')
                );
                $controller = new HomeController($this);
                echo $controller->index($this->request);
            } else {
                throw new PathNotFoundException('Page not found');
            }
        } catch (Exception $e) {
            //render nice exception
            $controller = new ExceptionController($this);
            echo $controller->render($this->request, $e);
        }
    }

    /**
     * @return PDORepository|null
     */
    public function getPdoRepository()
    {
        return $this->pdoRepository;
    }

    public static function config($key)
    {
        if (is_null(self::$config)){
            self::$config = self::getConfig();
        }

        return isset(self::$config[$key]) ? self::$config[$key] : null;
    }

    private static function getConfig()
    {
        return include (ROOT . 'config/app.php');
    }
}