<?php

namespace app\Controllers;

use app\Exceptions\Exception;
use app\Instruments\Request;
use app\Kernel;

class ExceptionController extends Controller
{
    /**
     * @param Request $request
     * @param Exception $exception
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public static function render(Request $request, Exception $exception)
    {
        $loader = new \Twig_Loader_Filesystem(Kernel::config('TWIG_VIEW_FOLDER'));
        $twig = new \Twig_Environment($loader);
        http_response_code($exception->getCode());
        return $twig->render('error.html.twig', array('text' => $exception->getMessage(), 'code' => $exception->getCode()));
    }
}