<?php

namespace app\Controllers;


use app\Exceptions\Exception;
use app\Kernel;

abstract class Controller
{
    protected $kernel;

    /**
     * Controller constructor.
     * @param Kernel $kernel
     * @throws Exception
     */
    public function __construct(Kernel $kernel)
    {
        $this->kernel = $kernel;
    }
}