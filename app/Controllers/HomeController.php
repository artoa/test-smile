<?php

namespace app\Controllers;

use app\Exceptions\DBException;
use app\Instruments\Request;
use app\Kernel;
use app\Models\Product;
use app\Models\Warehouse;
use app\Models\WarehouseContent;
use ParseCsv\Csv;

class HomeController extends Controller
{
    public function __construct(Kernel $kernel)
    {
        parent::__construct($kernel);

        if (is_null($this->kernel->getPdoRepository())) {
            throw new DBException();
        }
    }

    public function index(Request $request)
    {
        if ($request->getMethod() == 'POST'){
            $this->processFile($request);

            header("HTTP/1.1 301 Moved Permanently");
            header("Location: /", true, 301);
            return;
        }

        //message beg
        if ($value = $_SESSION['message']) {
            $viewParameters['message'] = $value;
            unset($_SESSION['message']);
        }

        if ($value = $_SESSION['error']) {
            $viewParameters['error'] = $value;
            unset($_SESSION['error']);
        }

        //table data
        $viewParameters['products'] = $this->getProductsCollection();

        //view
        $loader = new \Twig_Loader_Filesystem(Kernel::config('TWIG_VIEW_FOLDER'));
        $twig = new \Twig_Environment($loader);
        echo $twig->render('index.html.twig', $viewParameters);
    }

    /**
     * @return array
     * @throws DBException
     */
    private function getProductsCollection()
    {
        $productTable = Product::getTable();
        $warehouseTable = Warehouse::getTable();
        $warehouseContentTable = WarehouseContent::getTable();

        $sql = "SELECT $productTable.`id`, $productTable.`title`, $warehouseContentTable.`quantity`, $warehouseTable.`title` as `warehouse` 
            FROM $productTable 
            INNER JOIN $warehouseContentTable on $warehouseContentTable.`product_id` = $productTable.`id` AND $warehouseContentTable.`quantity` > 0
            INNER JOIN $warehouseTable on $warehouseContentTable.`warehouse_id` = $warehouseTable.`id`
            WHERE 1";

        $stmt = $this->kernel->getPdoRepository()->queryList($sql, []);
        //$stmt->execute();

        $result = [];
        while($row = $stmt->fetchObject()){
            if (!isset($result[$row->id])) {
                $result[$row->id] = [
                    'id' => $row->id,
                    'title' => $row->title,
                    'quantity' => 0,
                    'warehouse' => []
                ];
            }

            $result[$row->id]['quantity'] += $row->quantity;
            $result[$row->id]['warehouse'][] = $row->warehouse;
        }
        return $result;
    }

    private function processFile(Request $request)
    {
        $value = $request->getFileElement('csv');
        if (!$value || $value['error'] !== 0)
            return;

        if ($value['type'] != 'text/csv') {
            $_SESSION['error'] = 'Invalid file type';
            return;
        }

        //parse
        $csv = new Csv();
        $csv->auto($value['tmp_name']);

        //validation first row
        if (!$this->validCSV($csv)){
            $_SESSION['error'] = 'Invalid file content';
            return;
        }

        $this->processCSV($csv);
    }

    private function validCSV(Csv $csv)
    {
        return $csv->data && isset($csv->data[0]) && isset($csv->data[0]['product_name']) && isset($csv->data[0]['qty']) && isset($csv->data[0]['warehouse']);
    }

    /**
     * @param Csv $csv
     * @throws DBException
     */
    private function processCSV(Csv $csv)
    {
        $productsCollection = [];
        $warehouseCollection = [];
        $quantityCollection = [];

        foreach ($csv->data as $row) {
            if ($row['qty'] == 0) //don`t need any action in zero deviation
                continue;

            //get active product
            if (!isset($productsCollection[$row['product_name']])) {
                //search
                $product = Product::findOneByTitle($this->kernel->getPdoRepository(), $row['product_name']);

                if (!$product) { //create product
                    $product = new Product(['title' => $row['product_name']]);
                    $product->save($this->kernel->getPdoRepository());
                }

                $productsCollection[$row['product_name']] = $product;
            }

            /**
             * @var Product $product
             */
            $product = $productsCollection[$row['product_name']];

            //get active warehouse
            if (!isset($warehouseCollection[$row['warehouse']])) {
                //search
                $warehouse = Warehouse::findOneByTitle($this->kernel->getPdoRepository(), $row['warehouse']);

                if (!$warehouse) { //create item
                    $warehouse = new Warehouse(['title' => $row['warehouse']]);
                    $warehouse->save($this->kernel->getPdoRepository());
                }
                $warehouseCollection[$row['warehouse']] = $warehouse;
            }
            /**
             * @var Warehouse $warehouse
             */
            $warehouse = $warehouseCollection[$row['warehouse']];

            //find quantity
            $key = $this->getQuantityCollectionKey($product, $warehouse);
            if (!isset($quantityCollection[$key])) {
                //search
                $warehouseContent = WarehouseContent::findOneByIDs($this->kernel->getPdoRepository(), $product->getId(), $warehouse->getId());

                if (!$warehouseContent) {
                    $warehouseContent = new WarehouseContent([
                        'product_id' => $product->getId(),
                        'warehouse_id' => $warehouse->getId(),
                        'quantity' => 0
                    ]);
                }

                $quantityCollection[$key] = $warehouseContent;
            }

            /**
             * @var WarehouseContent $warehouseContent
             */
            //active content
            $warehouseContent = $quantityCollection[$key];
            $warehouseContent->addQuantity($row['qty']);
            $warehouseContent->save($this->kernel->getPdoRepository());
        }
        $_SESSION['message'] = 'Success parse';
    }

    private function getQuantityCollectionKey(Product $product, Warehouse $warehouse)
    {
        return "{$product->getId()}-{$warehouse->getId()}";
    }
}