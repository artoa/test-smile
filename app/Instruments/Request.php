<?php

namespace app\Instruments;

class Request
{
    /**
     * Request body parameters (POST).
     */
    public $request;

    /**
     * Query string parameters (GET)
     */
    public $query;

    /**
     * Headers (taken from the $_SERVER).
     */
    public $headers;

    /**
     * parameters from $_SERVER.
     */
    public $server;

    /**
     * @var string
     */
    protected $method;

    /**
     * paremeters from $_FILE
     */
    protected $files;

    public function __construct($createFromGlobals = true)
    {
        if($createFromGlobals){
            $this->createFromGlobals();
        }
    }

    public function createFromGlobals(){
        $this->setQuery($_GET);
        $this->setRequest($_POST);
        $this->setHeaders(apache_request_headers());
        $this->setMethod(isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : null);
        $this->setServer($_SERVER);
        $this->setFiles($_FILES);

        if($this->getMethod() != 'GET'){
            parse_str(file_get_contents('php://input'),$a);
            if($a){
                $this->setRequest(
                    array_merge($a, $this->request)
                );
            }
        }
    }

    /**
     * @var string $key
     * @return mixed
     */
    public function getRequestElement($key)
    {
        return isset($this->request[$key]) ? $this->request[$key] : null;
    }

    /**
     * @param mixed $request
     */
    public function setRequest($request)
    {
        $this->request = $request;
    }

    /**
     * @var string $key
     * @return mixed
     */
    public function getQueryElement($key)
    {
        return isset($this->query[$key]) ? $this->query[$key] : null;
    }

    /**
     * @param mixed $query
     */
    public function setQuery($query)
    {
        $this->query = $query;
    }

    /**
     * @var string $key
     * @return mixed
     */
    public function getHeadersElement($key)
    {
        return isset($this->headers[$key]) ? $this->headers[$key] : null;
    }

    /**
     * @param mixed $headers
     */
    public function setHeaders($headers)
    {
        $this->headers = $headers;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param string $method
     */
    public function setMethod($method)
    {
        $this->method = $method;
    }

    /**
     * @param string $key
     * @return null
     */
    public function getServerElement($key)
    {
        return isset($this->server[$key]) ? $this->server[$key] : null;
    }

    /**
     * @param mixed $server
     */
    public function setServer($server)
    {
        $this->server = $server;
    }

    /**
     * @return mixed
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * @param mixed $files
     */
    public function setFiles($files)
    {
        $this->files = $files;
    }

    /**
     * @var string $key
     * @return mixed
     */
    public function getFileElement($key)
    {
        return isset($this->files[$key]) ? $this->files[$key] : null;
    }
}