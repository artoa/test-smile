<?php

namespace app\Instruments;

use app\Exceptions\DBException;

class PDORepository
{
    private $username;
    private $port;
    private $password;
    private $host;
    private $db;
    private $connection;

    /**
     * PDORepository constructor.
     * @param $username
     * @param $port
     * @param $password
     * @param $host
     * @param $db
     */
    public function __construct($host, $port, $db, $username, $password)
    {
        $this->username = $username;
        $this->port = $port;
        $this->password = $password;
        $this->host = $host;
        $this->db = $db;
    }

    /**
     * Greate PDO connection
     * @return \PDO
     */
    private function getConnection()
    {
        if (is_null($this->connection)) {
            $this->connection = new \PDO("mysql:dbname={$this->db};port={$this->port};host={$this->host}", $this->username, $this->password);
        }
        return $this->connection;
    }

    /**
     * @param $sql
     * @param $args
     * @return bool|\PDOStatement
     * @throws DBException
     */
    public function queryList($sql, $args)
    {
        try {
            $connection = $this->getConnection();
            $stmt = $connection->prepare($sql);
            $stmt->execute($args);
            return $stmt;
        } catch (\PDOException $exception) {
            throw new DBException($exception->getMessage());
        }
    }

    /**
     * @param $sql
     * @param $args
     * @return \PDO
     * @throws DBException
     */
    public function insertQuery($sql, $args)
    {
        try {
            $connection = $this->getConnection();
            $stmt = $connection->prepare($sql);
            $stmt->execute($args);
            return $connection;
        } catch (\PDOException $exception) {
            throw new DBException($exception->getMessage());
        }
    }
}