<?php

namespace app\Models;

use app\Exceptions\DBException;
use app\Instruments\PDORepository;

class Warehouse implements Model
{
    private $id;

    private $title;

    public function __construct($params = [])
    {
        if (isset($params['id'])) {
            $this->setId($params['id']);
        }

        if (isset($params['title'])) {
            $this->setTitle($params['title']);
        }
    }


    public static function getTable()
    {
        return '`warehouse`';
    }

    public static function findOneByTitle(PDORepository $repository, $title)
    {
        $sql = 'SELECT * FROM ' . self::getTable() . ' WHERE `title` = :title LIMIT 0, 1';
        $smtp = $repository->queryList($sql, [':title'=>$title]);
        if ($row = $smtp->fetch()) {
            return new self($row);
        }
        return null;
    }

    /**
     * @param PDORepository $repository
     * @throws DBException
     */
    public function save(PDORepository $repository)
    {
        if ($this->id) {
            //update
            $sql = 'UPDATE ' . self::getTable() . ' SET `title` = :title WHERE `id` = :id';
            $repository->queryList($sql, [':title' => $this->getTitle(), ':id' => $this->getId()]);
        } else {
            //insert
            $sql = 'INSERT INTO ' . self::getTable() .' (`id`, `title`) VALUES (NULL, :title)';
            $connection = $repository->insertQuery($sql, [':title' => $this->getTitle()]);
            if ($value = $connection->lastInsertId()) {
                $this->setId($value);
            } else {
                throw new DBException('Warehouse create error');
            }
        }
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    private function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }
}