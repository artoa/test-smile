<?php

namespace app\Models;

use app\Exceptions\DBException;
use app\Instruments\PDORepository;

class WarehouseContent
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $product_id;

    /**
     * @var integer
     */
    private $warehouse_id;

    /**
     * @var integer
     */
    private $quantity;

    public function __construct($params = [])
    {
        if (isset($params['id'])) {
            $this->setId($params['id']);
        }
        if (isset($params['product_id'])) {
            $this->setProductId($params['product_id']);
        }
        if (isset($params['warehouse_id'])) {
            $this->setWarehouseId($params['warehouse_id']);
        }
        if (isset($params['quantity'])) {
            $this->setQuantity($params['quantity']);
        }
    }


    public static function getTable()
    {
        return '`warehouse_content`';
    }

    public static function findOneByIDs(PDORepository $repository, $productId, $warehouseId)
    {
        $sql = 'SELECT * FROM ' . self::getTable() . ' WHERE `product_id` = :product_id AND `warehouse_id` = :warehouse_id LIMIT 0, 1';
        $smtp = $repository->queryList($sql, [':product_id'=>$productId, ':warehouse_id'=> $warehouseId]);
        if ($row = $smtp->fetch()) {
            return new self($row);
        }
        return null;
    }

    /**
     * @param PDORepository $repository
     * @throws DBException
     */
    public function save(PDORepository $repository)
    {
        if ($this->id) {
            //update
            $sql = 'UPDATE ' . self::getTable() . ' SET `quantity` = :quantity WHERE `id` = :id';
            $repository->queryList($sql, [':quantity' => $this->getQuantity(), ':id' => $this->getId()]);
        } else {
            //insert
            $sql = 'INSERT INTO ' . self::getTable() .' (`id`, `product_id`, `warehouse_id`, `quantity`) 
                VALUES (NULL, :product_id, :warehouse_id, :quantity)';
            $connection = $repository->insertQuery(
                $sql,
                [
                    ':product_id' => $this->getProductId(),
                    ':warehouse_id'=> $this->getWarehouseId(),
                    ':quantity'=>$this->getQuantity()
                ]
            );
            if ($value = $connection->lastInsertId()) {
                $this->setId($value);
            } else {
                throw new DBException('Warehouse content create error');
            }
        }
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    private function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->product_id;
    }

    /**
     * @param mixed $product_id
     */
    private function setProductId($product_id)
    {
        $this->product_id = $product_id;
    }

    /**
     * @return mixed
     */
    public function getWarehouseId()
    {
        return $this->warehouse_id;
    }

    /**
     * @param mixed $warehouse_id
     */
    private function setWarehouseId($warehouse_id)
    {
        $this->warehouse_id = $warehouse_id;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @param $quantity
     */
    public function addQuantity($quantity)
    {
        $this->quantity += intval($quantity);
    }
}