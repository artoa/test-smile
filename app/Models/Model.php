<?php

namespace app\Models;

use app\Instruments\PDORepository;

interface Model
{
    public static function getTable();

    public function save(PDORepository $repository);
}